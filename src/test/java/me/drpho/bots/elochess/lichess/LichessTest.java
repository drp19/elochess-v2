package me.drpho.bots.elochess.lichess;

import me.drpho.bots.elochess.events.GameProcessor;
import me.drpho.bots.elochess.models.Game;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LichessTest {

	@Test
	void getGame() throws GameProcessor.ParseError {
		assertEquals(Lichess.getGame("xwj83zQeasgwsgrewg"), Lichess.getGame("xwj83zQe"));

		Lichess.LichessGame result = Lichess.getGame("xwj83zQe");
		assertEquals(result.getId(), "xwj83zQe");
		assertEquals(result.getWhite(), "drnykterstein");
		assertEquals(result.getBlack(), "rebeccaharris");
		assertEquals(result.getMode(), "bullet");
		assertEquals(result.getTimeStr(), "1|0");
		assertEquals(result.getResult(), Game.EndType.MATE);
		assertEquals(result.getWinner(), Game.Winner.BLACK);
		assertEquals(result.getTimestamp(), 1588633619006L);

		result = Lichess.getGame("GweEjsid");
		assertEquals(result.getId(), "GweEjsid");
		assertEquals(result.getWhite(), "drp19");
		assertEquals(result.getBlack(), "hershalrami");
		assertEquals(result.getMode(), "untimed");
		assertNull(result.getTimeStr());
		assertEquals(result.getResult(), Game.EndType.MATE);
		assertEquals(result.getWinner(), Game.Winner.BLACK);
		assertEquals(result.getTimestamp(), 1581059636083L);

		try {
			Lichess.getGame("egfhwgiuwgewreg");
			fail("expected error");
		} catch (GameProcessor.ParseError ignored) {}
	}

	@Test
	void checkAcount() {
		assertTrue(Lichess.checkAccount("drp19"));
		assertFalse(Lichess.checkAccount("238ih5tiu4fghv"));
	}
}