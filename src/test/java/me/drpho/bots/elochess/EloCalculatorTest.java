package me.drpho.bots.elochess;

import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Mode;
import me.drpho.bots.elochess.models.Player;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class EloCalculatorTest {

	@Test
	void calculateNewElo() {
		Map<String, Double> whiteMap = new HashMap<>(), blackMap = new HashMap<>();
		whiteMap.put("blitz", (double) 1200);
		blackMap.put("blitz", (double) 1510);
		whiteMap.put("bullet", (double) 1200);
		blackMap.put("bullet", (double) 1200);
		Mode mode = new Mode(null, "blitz", null, true);
		Mode bullet = new Mode(null, "bullet", null, true);
		Mode unrated = new Mode(null, "blitz", null, false);
		Player white = new Player(null, null, null, null, null, null, whiteMap);
		Player black = new Player(null, null, null, null, null, null, blackMap);
		Game gameRated = new Game(null, null, null, null, null, null, mode, null, white.id, black.id,
				1200.0, 1510.0, Game.Winner.WHITE, Game.EndType.MATE, Game.Type.MANUAL, null, null);
		Game tied = new Game(null, null, null, null, null, null, bullet, null, white.id, black.id,
				1200.0, 1510.0, Game.Winner.NONE, Game.EndType.DRAW, Game.Type.MANUAL, null, null);

		EloCalculator.PlayerPair result = EloCalculator.calculateNewElo(new EloCalculator.PlayerPair(white, black), tied);
		assertEquals(result.white.getElo("bullet"), 1200);
		assertEquals(result.black.getElo("bullet"), 1200);
		result = EloCalculator.calculateNewElo(new EloCalculator.PlayerPair(white, black), gameRated);
		assertEquals(result.white.getElo("blitz"), 1225.7, .05);
		assertEquals(result.black.getElo("blitz"), 1484.3, .05);

		Game unRated = new Game(null, null, null, null, null, null, unrated, null, white.id, black.id,
				1200.0, 1510.0, Game.Winner.WHITE, Game.EndType.MATE, Game.Type.MANUAL, null, null);
		assertEquals(new EloCalculator.PlayerPair(white, black), EloCalculator.calculateNewElo(new EloCalculator.PlayerPair(white, black), unRated));
	}
}