package me.drpho.bots.elochess.events;

import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameProcessorTest {

	@Test
	void parseArgs() throws GameProcessor.ParseError {
		String cmd = "https://lichess.org/abc -m 5 a a //comment";
		GameProcessor.ParseResult result = GameProcessor.parseArgs(cmd);
		assertEquals(result.getWhite(), "a");
		assertEquals(result.getBlack(), "a");
		assertEquals(result.getWhiteSource(), Player.EntryType.NAME);
		assertEquals(result.getBlackSource(), Player.EntryType.NAME);
		assertEquals(result.getComment(), "comment");
		assertNull(result.getEndType());
		assertNull(result.getWinner());
		assertEquals(result.getLichessId(), "abc");
		assertEquals(result.getMode(), "5");
		assertEquals(result.getType(), Game.Type.LICHESS);

		cmd = "https://lichess.org/abc -m 5 //comment";
		result = GameProcessor.parseArgs(cmd);
		assertNull(result.getWhite());
		assertNull(result.getBlack());
		assertEquals(result.getWhiteSource(), Player.EntryType.LICHESS);
		assertEquals(result.getBlackSource(), Player.EntryType.LICHESS);
		assertEquals(result.getComment(), "comment");
		assertNull(result.getEndType());
		assertNull(result.getWinner());
		assertEquals(result.getLichessId(), "abc");
		assertEquals(result.getMode(), "5");
		assertEquals(result.getType(), Game.Type.LICHESS);

		cmd = "<@123456789012345678> <@123456789012345679> <@123456789012345678> -c abxcdfweigf";
		result = GameProcessor.parseArgs(cmd);
		assertEquals(result.getWhite(), "123456789012345678");
		assertEquals(result.getBlack(), "123456789012345679");
		assertEquals(result.getWhiteSource(), Player.EntryType.DISCORD);
		assertEquals(result.getBlackSource(), Player.EntryType.DISCORD);
		assertEquals(result.getComment(), "abxcdfweigf");
		assertEquals(result.getEndType(), Game.EndType.MATE);
		assertEquals(result.getWinner(), Game.Winner.WHITE);
		assertNull(result.getLichessId());
		assertNull(result.getMode());
		assertEquals(result.getType(), Game.Type.MANUAL);

		cmd = "<@123456789012345678> <@123456789012345679> <@123456789012315678> -c abxcdfweigf";
		try {
			GameProcessor.parseArgs(cmd);
		} catch (GameProcessor.ParseError p) {
			assertEquals(p, GameProcessor.ParseError.PATTERN_ERROR);
		}

		cmd = "-m 15 abc abd abd -r abxcdfweigf";
		result = GameProcessor.parseArgs(cmd);
		assertEquals(result.getWhite(), "abc");
		assertEquals(result.getBlack(), "abd");
		assertEquals(result.getWhiteSource(), Player.EntryType.NAME);
		assertEquals(result.getBlackSource(), Player.EntryType.NAME);
		assertEquals(result.getComment(), "abxcdfweigf");
		assertEquals(result.getEndType(), Game.EndType.RESIGN);
		assertEquals(result.getWinner(), Game.Winner.BLACK);
		assertNull(result.getLichessId());
		assertEquals(result.getMode(), "15");
		assertEquals(result.getType(), Game.Type.MANUAL);

		cmd = "-m 15 abc abd abd abxcdfweigf";
		result = GameProcessor.parseArgs(cmd);
		assertEquals(result.getEndType(), Game.EndType.MATE);

		cmd = "-m 15 abc abd draw abxcdfweigf";
		result = GameProcessor.parseArgs(cmd);
		assertEquals(result.getEndType(), Game.EndType.DRAW);
	}
}