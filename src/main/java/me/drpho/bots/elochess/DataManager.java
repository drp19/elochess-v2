package me.drpho.bots.elochess;

import discord4j.core.object.entity.Guild;
import lombok.NonNull;
import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Mode;
import me.drpho.bots.elochess.models.Player;
import me.drpho.bots.elochess.models.Server;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class DataManager {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private static Connection conn;

	/**
	 * Connect to the database
	 * @param fileName the database file name
	 */
	public static void connect(String fileName) throws SQLException {
		File f = new File(fileName);
		String url = "jdbc:sqlite:" + f.getAbsolutePath();
		try {
			conn = DriverManager.getConnection(url);
			logger.debug("Database driver: {}", conn.getMetaData().getDriverName());
			logger.info("Database connection established.");
			createTables();
		} catch (SQLException e) {
			logger.error("SQL Connect Error: {}", e.getMessage());
			throw e;
		}
	}

	/**
	 * Returns the player referenced by the parameter, or create a new player
	 * @param id A discord ID
	 * @return Player object
	 */
	@NonNull
	public static Player getPlayerByDiscordId(String guildId, String id) throws SQLException {
		Player p = getPlayer("discord_id", id, guildId);
		if (p == null) {
			createPlayer(guildId, "discord_id", id);
			return getPlayer("discord_id", id, guildId);
		}
		return p;
	}

	/**
	 * Returns the player referenced by the parameter, or create a new player
	 * @param name A player name
	 * @return Player object
	 */
	@NonNull
	public static Player getPlayerByName(String guildId, String name) throws SQLException {
		Player p = getPlayer("plain_name", name, guildId);
		if (p == null) {
			createPlayer(guildId, "plain_name", name);
			return getPlayer("plain_name", name, guildId);
		}
		return p;
	}

	/**
	 * Returns the player referenced by the parameter, or create a new player
	 * @param id Lichess username
	 * @return Player object
	 */
	@NonNull
	public static Player getPlayerByLichessId(String guildId, String id) throws SQLException {
		Player p = getPlayer("lichess_id", id, guildId);
		if (p == null) {
			createPlayer(guildId, "lichess_id", id);
			return getPlayer("lichess_id", id, guildId);
		}
		return p;
	}

	public static boolean playerExists(String guildId, Player.EntryType type, String value) {
		switch (type) {
			case DISCORD: return getPlayer("discord_id", value, guildId) != null;
			case NAME: return getPlayer("plain_name", value, guildId) != null;
			case LICHESS: return getPlayer("lichess_id", value, guildId) != null;
			default: return false;
		}
	}

	private static Player getPlayer(String field, String param, String guildId) {
		String sql = String.format("SELECT * "
				+ "FROM players WHERE guild_id = ? AND UPPER(%s) = ?;", field);
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			pstmt.setString(2, param.toUpperCase());
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return new Player(rs.getInt("id"), rs.getString("guild_id"), rs.getString("discord_id"),
						rs.getString("plain_name"), rs.getString("lichess_id"), rs.getString("chessgame_id"), mapFromEncodedString(rs.getString("elo_array")));
			}
			return null;
		} catch (SQLException e) {
			logger.error("General SQL error: {}", e.getMessage());
			return null;
		}
	}

	private static Player getPlayer(int id) {
		String sql = "SELECT * "
				+ "FROM players WHERE id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return new Player(rs.getInt("id"), rs.getString("guild_id"), rs.getString("discord_id"),
						rs.getString("plain_name"), rs.getString("lichess_id"), rs.getString("chessgame_id"), mapFromEncodedString(rs.getString("elo_array")));
			}
			return null;
		} catch (SQLException e) {
			logger.error("General SQL error: {}", e.getMessage());
			return null;
		}
	}

	public static int gamesPlayed(int id) {
		String sql = "SELECT COUNT(id) "
				+ "FROM games WHERE white_id = ? OR black_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			pstmt.setInt(2, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} catch (SQLException e) {
			logger.error("General SQL error: {}", e.getMessage());
			return 0;
		}
	}

	private static void createPlayer(String guildId, String field, String value) throws SQLException {
		String insertSQL = String.format("INSERT INTO players(guild_id, %s) VALUES(?, ?);", field);
		try {
			PreparedStatement insertServer = conn.prepareStatement(insertSQL);
			insertServer.setString(1, guildId);
			insertServer.setString(2, value);
			insertServer.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert Error: {}", e.getMessage());
			throw e;
		}
	}

	public static void updatePlayer(int id, String field, String value) throws SQLException {
		String insertSQL = String.format("UPDATE players SET %s = ?\n" +
				"WHERE id = ?;", field);
		try {
			PreparedStatement insertServer = conn.prepareStatement(insertSQL);
			insertServer.setString(1, value);
			insertServer.setInt(2, id);
			insertServer.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert Error: {}", e.getMessage());
			throw e;
		}
	}

	public static int createPlayer(Player p) throws SQLException {
		String insertSQL = "INSERT INTO players(guild_id, discord_id, plain_name, lichess_id, chessgame_id, elo_array) VALUES(?, ?, ?, ?, ?, ?);";
		try {
			PreparedStatement insertServer = conn.prepareStatement(insertSQL, Statement.RETURN_GENERATED_KEYS);
			insertServer.setString(1, p.guildId);
			insertServer.setString(2, p.discordId);
			insertServer.setString(3, p.name);
			insertServer.setString(4, p.lichess);
			insertServer.setString(5, p.chessgame);
			insertServer.setString(6, encodedStringFromMap(p.getEloMap()));
			int res = insertServer.executeUpdate();
			if (res == 0) {
				throw new SQLException("Creating player failed");
			}
			try (ResultSet generatedKeys = insertServer.getGeneratedKeys()) {
				if (generatedKeys.next()) {
					return generatedKeys.getInt("id");
				}
				else {
					throw new SQLException("Creating user failed, no ID obtained.");
				}
			}
		} catch (SQLException e) {
			logger.error("SQL Insert Error: {}", e.getMessage());
			throw e;
		}
	}

	@NonNull
	public static Optional<Mode> getModeByName(@Nullable String guildId, String name) {
		String sql = "SELECT * "
				+ "FROM modes WHERE (guild_id = ? OR guild_id IS NULL) AND UPPER(plain_name) = ?;";

		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			pstmt.setString(2, name.toUpperCase());
			ResultSet rs  = pstmt.executeQuery();

			if (rs.next()) {
				return Optional.of(new Mode(guildId, name, rs.getString("description"), rs.getBoolean("rated")));
			} else {
				return Optional.empty();
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
			return Optional.empty();
		}
	}

	public static void addMode(Mode mode) throws SQLException {
		String insertSQL = "INSERT INTO modes(guild_id, plain_name, description, rated) VALUES(?, ?, ?, ?);";

		if (!getModeByName(mode.guildId, mode.name).isPresent()) {
			try {
				PreparedStatement insertServer = conn.prepareStatement(insertSQL);
				insertServer.setString(1, mode.guildId);
				insertServer.setString(2, mode.name);
				insertServer.setString(3, mode.description);
				insertServer.setBoolean(4, mode.rated);
				insertServer.executeUpdate();
			} catch (SQLException e) {
				logger.error("SQL Insert Error: {}", e.getMessage());
				throw e;
			}
		}
	}

	public static void addGame(Game game) throws SQLException {
		String insertSQL = "INSERT INTO games(guild_id, game_time, create_message_id, author_id, status, mode, time_str, white_id, black_id, white_prev_elo, black_prev_elo, winner, end_type, game_type, link, comment) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

		try {
			PreparedStatement insertGame = conn.prepareStatement(insertSQL);
			insertGame.setString(1, game.getGuildId());
			insertGame.setLong(2, game.getTimestamp().toEpochMilli());
			insertGame.setString(3, game.getCreateMessage());
			insertGame.setString(4, game.getAuthorId());
			insertGame.setInt(5, game.getStatus().id);
			insertGame.setString(6, game.getMode().name);
			insertGame.setString(7, game.getTimeStr());
			insertGame.setInt(8, game.getWhite());
			insertGame.setInt(9, game.getBlack());
			insertGame.setDouble(10, game.getWhitePriorElo());
			insertGame.setDouble(11, game.getBlackPriorElo());
			insertGame.setInt(12, game.getWinner().id);
			insertGame.setInt(13, game.getEndType().id);
			insertGame.setInt(14, game.getType().id);
			insertGame.setString(15, game.getLink());
			insertGame.setString(16, game.getComment());
			insertGame.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert Error: {}", e.getMessage());
			throw e;
		}
	}

	public static Game getGame(int id) {
		String sql = "SELECT * "
				+ "FROM games WHERE id = ?;";

		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				return gameFromResult(rs);
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
		}
		return null;
	}

	public static List<Game> getAllGames(String guildId) {
		String sql = "SELECT * "
				+ "FROM games WHERE guild_id = ?;";
		List<Game> games = new ArrayList<>();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				games.add(gameFromResult(rs));
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
		}
		return games;
	}

	public static List<Player> getAllPlayers(String guildId) {
		String sql = "SELECT * "
				+ "FROM players WHERE guild_id = ?;";
		List<Player> players = new ArrayList<>();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				players.add(new Player(rs.getInt("id"), rs.getString("guild_id"), rs.getString("discord_id"),
						rs.getString("plain_name"), rs.getString("lichess_id"), rs.getString("chessgame_id"), mapFromEncodedString(rs.getString("elo_array"))));
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
		}
		return players;
	}

	public static String getPlayerString(Mono<Guild> g, int id) {
		String sql = "SELECT * "
				+ "FROM players WHERE id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Player p = new Player(rs.getInt("id"), rs.getString("guild_id"), rs.getString("discord_id"),
						rs.getString("plain_name"), rs.getString("lichess_id"), rs.getString("chessgame_id"), mapFromEncodedString(rs.getString("elo_array")));
				return p.getPrint(g);
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
		}
		return null;
	}
	public static Map<Integer, String> getAllPlayerString(String guildId, Mono<Guild> g) {
		String sql = "SELECT * "
				+ "FROM players WHERE guild_id = ?;";
		Map<Integer, String> players = new HashMap<>();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Player p = new Player(rs.getInt("id"), rs.getString("guild_id"), rs.getString("discord_id"),
						rs.getString("plain_name"), rs.getString("lichess_id"), rs.getString("chessgame_id"), mapFromEncodedString(rs.getString("elo_array")));
				players.put(p.id, p.getPrint(g));
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
		}
		return players;
	}

	public static List<Mode> getModes(String guildId) {
		String sql = "SELECT * "
				+ "FROM modes WHERE guild_id = ? OR guild_id IS NULL;";
		List<Mode> modes = new ArrayList<>();
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				modes.add(new Mode(guildId, rs.getString("plain_name"), rs.getString("description"), rs.getBoolean("rated")));
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
		}
		return modes;
	}

	private static Game gameFromResult(ResultSet rs) throws SQLException {
		Optional<Mode> m = getModeByName(rs.getString("guild_id"), rs.getString("mode"));
		Optional<Game.Status> status = Game.Status.valueOf(rs.getInt("status"));
		Optional<Game.Winner> winner = Game.Winner.valueOf(rs.getInt("winner"));
		Optional<Game.EndType> endType = Game.EndType.valueOf(rs.getInt("end_type"));
		Optional<Game.Type> type = Game.Type.valueOf(rs.getInt("game_type"));
		if (!(m.isPresent() && status.isPresent() && winner.isPresent() && endType.isPresent() && type.isPresent())) {
			return null;
		}
		return new Game(rs.getInt("id"), rs.getString("guild_id"), Instant.ofEpochMilli(rs.getLong("game_time")),
				rs.getString("create_message_id"), rs.getString("author_id"), status.get(), m.get(),
				rs.getString("time_str"), rs.getInt("white_id"), rs.getInt("black_id"),
				rs.getDouble("white_prev_elo"), rs.getDouble("black_prev_elo"), winner.get(),
				endType.get(), type.get(), rs.getString("link"), rs.getString("comment"));
	}

	public static boolean gameExistsWithLink(String link) {
		String sql = "SELECT * "
				+ "FROM games WHERE link = ?;";

		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, link);
			ResultSet rs  = pstmt.executeQuery();

			return rs.next();
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
			return false;
		}
	}

	public static void updatePlayerElo(Player p) throws SQLException {
		String updateElo = "UPDATE players SET elo_array = ?\n" +
				"WHERE id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(updateElo);
			pstmt.setString(1, encodedStringFromMap(p.getEloMap()));
			pstmt.setInt(2, p.id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert error: {}", e.getMessage());
			throw e;
		}
	}

	public static void clearAllElo(String guildId) throws SQLException {
		String clear = "UPDATE players SET elo_array = NULL\n" +
				"WHERE guild_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(clear);
			pstmt.setString(1, guildId);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert error: {}", e.getMessage());
			throw e;
		}
	}

	public static void updateGameElo(int id, double white, double black) throws SQLException {
		String updateElo = "UPDATE games SET white_prev_elo = ?, black_prev_elo = ?\n" +
				"WHERE id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(updateElo);
			pstmt.setDouble(1, white);
			pstmt.setDouble(2, black);
			pstmt.setInt(3, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert error: {}", e.getMessage());
			throw e;
		}
	}

	public static void recalcuateGuildData(String guildId) throws SQLException {
		clearAllElo(guildId);
		List<Game> games = getAllGames(guildId);
		games.sort(Comparator.comparingInt(Game::getId));
		for (Game g : games) {
			Player white = getPlayer(g.getWhite()), black = getPlayer(g.getBlack());
			updateGameElo(g.getId(), white.getElo(g.getMode().name), black.getElo(g.getMode().name));
			EloCalculator.PlayerPair newPlayers = EloCalculator.calculateNewElo(new EloCalculator.PlayerPair(white, black), g);
			DataManager.updatePlayerElo(newPlayers.white);
			DataManager.updatePlayerElo(newPlayers.black);
		}
	}

	public static void replacePlayer(int from, int to) throws SQLException {
		String updateEloWhite = "UPDATE games SET white_id = ?\n" +
				"WHERE white_id = ?;";
		String updateEloBlack = "UPDATE games SET black_id = ?\n" +
				"WHERE black_id = ?;";
		String removePlayer = "DELETE FROM players WHERE id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(updateEloWhite);
			pstmt.setInt(1, to);
			pstmt.setInt(2, from);
			pstmt.executeUpdate();
			pstmt = conn.prepareStatement(updateEloBlack);
			pstmt.setInt(1, to);
			pstmt.setInt(2, from);
			pstmt.executeUpdate();
			pstmt = conn.prepareStatement(removePlayer);
			pstmt.setInt(1, from);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Insert error: {}", e.getMessage());
			throw e;
		}
	}

	public static void joinServer(String guildId) throws SQLException {
		String insertSQL = "INSERT INTO servers(guild_id, first_seen) VALUES(?, ?);";

		if (getServer(guildId) == null) {
			try {
				PreparedStatement insertServer = conn.prepareStatement(insertSQL);
				insertServer.setString(1, guildId);
				insertServer.setLong(2, Instant.now().getEpochSecond());
				insertServer.executeUpdate();
			} catch (SQLException e) {
				logger.error("SQL Insert Error: {}", e.getMessage());
				throw e;
			}
		}
	}

	@Nullable
	public static Server getServer(String guildId) {
		String sql = "SELECT * "
				+ "FROM servers WHERE guild_id = ?;";

		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			ResultSet rs  = pstmt.executeQuery();

			if (rs.next()) {
				List<String> channels;
				if (rs.getString("channels") == null) {
					channels = new ArrayList<>();
				} else {
					channels = Arrays.asList(rs.getString("channels").split(","));
				}
				return new Server(rs.getString("guild_id"), rs.getBoolean("allow_delete"), rs.getBoolean("allow_add_mode"),
						rs.getBoolean("allow_all_channels"), channels, rs.getString("default_mode"), rs.getString("prefix"));
			} else {
				return null;
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
			return null;
		}
	}

	private static String encodedStringFromMap(Map<String, Double> map) {
		Base64.Encoder encoder = Base64.getEncoder();
		return map.keySet().stream()
				.filter(s -> map.get(s) != null)
				.map(key -> encoder.encodeToString(key.getBytes()) + ">" + map.get(key))
				.collect(Collectors.joining(","));
	}

	private static Map<String, Double> mapFromEncodedString(String str) {
		Base64.Decoder decoder = Base64.getDecoder();
		if (str == null) {
			return new HashMap<>();
		}
		return Arrays.stream(str.split(","))
				.map(entry -> entry.split(">"))
				.collect(Collectors.toMap(entry -> new String(decoder.decode(entry[0])), entry -> Double.parseDouble(entry[1])));
	}

	private static void createTables() throws SQLException {
		String createGames = "CREATE TABLE IF NOT EXISTS games (\n"
				+ "	id INTEGER PRIMARY KEY,\n"
				+ "	guild_id TEXT NOT NULL,\n"
				+ " game_time INT NOT NULL,\n"
				+ "	create_message_id TEXT UNIQUE NOT NULL,\n"
				+ " author_id TEXT NOT NULL,\n"
				+ " status INT DEFAULT 0,\n"
				+ " mode TEXT,\n"
				+ " time_str TEXT,\n"
				+ " white_id INT NOT NULL,\n"
				+ " black_id INT NOT NULL,\n"
				+ " white_prev_elo DOUBLE,\n"
				+ " black_prev_elo DOUBLE,\n"
				+ " winner INT NOT NULL,\n"
				+ " end_type INT NOT NULL,\n"
				+ " game_type INT NOT NULL,\n"
				+ " link TEXT,\n"
				+ " comment TEXT\n"
				+ ");";

		String createPlayers = "CREATE TABLE IF NOT EXISTS players (\n"
				+ "	id INTEGER PRIMARY KEY,\n"
				+ "	guild_id TEXT NOT NULL,\n"
				+ " discord_id TEXT,\n"
				+ " plain_name TEXT,\n"
				+ " lichess_id TEXT,\n"
				+ " chessgame_id TEXT,\n"
				+ " elo_array TEXT\n"
				+ ");";

		String createServers = "CREATE TABLE IF NOT EXISTS servers (\n"
				+ "	guild_id TEXT PRIMARY KEY,\n"
				+ " first_seen INT NOT NULL,\n"
				+ " default_mode TEXT,\n"
				+ " allow_delete NUMERIC DEFAULT TRUE,\n"
				+ " allow_add_mode NUMERIC DEFAULT TRUE,\n"
				+ " allow_all_channels NUMERIC DEFAULT TRUE,\n"
				+ " channels TEXT,\n"
				+ " prefix TEXT,\n"
				+ " lang TEXT\n"
				+ ");";

		String createModes = "CREATE TABLE IF NOT EXISTS modes (\n"
				+ "	guild_id TEXT PRIMARY KEY,\n"
				+ " plain_name TEXT NOT NULL,\n"
				+ " description TEXT,\n"
				+ " rated NUMERIC DEFAULT TRUE\n"
				+ ");";

		Statement stmt = conn.createStatement();
		// create a new table
		stmt.execute(createGames);
		stmt.execute(createServers);
		stmt.execute(createPlayers);
		stmt.execute(createModes);
	}

	public static void main (String[] args) throws SQLException {
//		Mode BULLET = new Mode(null, "Bullet", "Games less than 3minutes.", true);
//		Mode BLITZ = new Mode(null, "Blitz", "Games between 3 and 10 minutes.", true);
//		Mode RAPID = new Mode(null, "Rapid", "Games between 10 and 60 minutes.", true);
//		Mode UNTIMED = new Mode(null, "Untimed", "Untimed games.", true);
//		Mode CASUAL = new Mode(null, "Casual", "Casual games with no ELO.", false);

		connect("bot.db");

		recalcuateGuildData("323268614939475969");
//		addMode(BLITZ);
//		addMode(BULLET);
//		addMode(RAPID);
//		addMode(UNTIMED);
//		addMode(CASUAL);
	}

}
