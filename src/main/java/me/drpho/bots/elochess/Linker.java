package me.drpho.bots.elochess;

import me.drpho.bots.elochess.models.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.SQLException;

public class Linker {

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	public static Player linkLichessWithDiscord(Player lichess, Player discord) {
		try {
			logger.info("Attempting to link {} and {}", lichess.id, discord.id);
			DataManager.updatePlayer(lichess.id, "discord_id", discord.discordId);
			DataManager.replacePlayer(discord.id, lichess.id);
			DataManager.recalcuateGuildData(lichess.guildId);
			return DataManager.getPlayerByDiscordId(lichess.guildId, discord.discordId);
		} catch (SQLException throwables) {
			return null;
		}
	}

	public static Player linkNameWithDiscord(Player name, Player discord) {
		try {
			logger.info("Attempting to link {} and {}", name.id, discord.id);
			DataManager.updatePlayer(name.id, "plain_name", discord.discordId);
			DataManager.replacePlayer(discord.id, name.id);
			DataManager.recalcuateGuildData(name.guildId);
			return DataManager.getPlayerByDiscordId(name.guildId, discord.discordId);
		} catch (SQLException throwables) {
			return null;
		}
	}

	public static Player linkLichessWithName(Player lichess, Player name) {
		try {
			logger.info("Attempting to link {} and {}", lichess.id, name.id);
			DataManager.updatePlayer(lichess.id, "plain_name", name.name);
			DataManager.replacePlayer(name.id, lichess.id);
			DataManager.recalcuateGuildData(lichess.guildId);
			return DataManager.getPlayerByName(lichess.guildId, name.name);
		} catch (SQLException throwables) {
			return null;
		}
	}
}
