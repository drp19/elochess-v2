package me.drpho.bots.elochess.events;

import discord4j.core.object.entity.Message;
import me.drpho.bots.elochess.models.Server;


public class EventHandler {

	public static void process(String commandStr, Server server, Message m, boolean isAdmin) {
		String[] parts = commandStr.split(" ", 2);
		String subCommand = parts[0];
		String args = parts.length > 1 ? parts[1] : "";
		switch (subCommand) {
			case "play": GameProcessor.processCommand(subCommand, args, server, m); break;
			case "games": case "game": case "top": HistoryProcessor.processCommand(subCommand, args, server, m);break;
			case "lichess": case "name": LinkProcessor.processCommand(subCommand, args, server, m, isAdmin);break;
			case "settings": case "modes": case "player": SettingsProcessor.processCommand(subCommand, args, server, m, isAdmin);break;
			default: HelpProcessor.processCommand(subCommand, args, server, m);break;
		}
	}

	public static void processReaction() {

	}
}
