package me.drpho.bots.elochess.events;

import discord4j.core.object.entity.Message;
import me.drpho.bots.elochess.DataManager;
import me.drpho.bots.elochess.EmbedCreator;
import me.drpho.bots.elochess.Linker;
import me.drpho.bots.elochess.lichess.Lichess;
import me.drpho.bots.elochess.models.Player;
import me.drpho.bots.elochess.models.Server;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkProcessor  {
	private static final Pattern ID_PATTERN = Pattern.compile("^<@!?(\\d{18})>$");

	public static void processCommand(String subCommand, String args, Server server, Message m, boolean isAdmin) {
		switch (subCommand) {
			case "lichess": linkLichess(args, server, m, isAdmin); break;
			case "name": linkName(args, server, m, isAdmin); break;
		}
	}

	private static void linkName(String args, Server server, Message message, boolean isAdmin) {
		String[] parts = args.split(" ");
		String linking = parts[0];
		String user = message.getAuthor().get().getId().asString();
		String response = null;
		if (parts.length >= 2) {
			if (!isAdmin) {
				response = "You do not have permission to set other people's links.";
			} else {
				Matcher m = ID_PATTERN.matcher(parts[1]);
				if (m.matches()) {
					user = m.group(1);
				} else {
					response = "Second argument is malformed.";
				}
			}
			if (response != null) {
				String finalResponse = response;
				message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
				return;
			}
		}

		try {
			Player discord = DataManager.getPlayerByDiscordId(server.guildId, user);
			Player name = DataManager.getPlayerByName(server.guildId, linking);
			if (discord.equals(name)) {
				response = "You've alrady linked those accounts.";
			} else if (discord.name != null) {
				response = String.format("You already have a name set. Use %s player to check your data.", server.prefix);
			} else if (name.discordId != null) {
				response = "Someone has already taken that name.";
			}
			if (response == null) {
				Linker.linkNameWithDiscord(name, discord);
				response = "Success.";
			}
		} catch (SQLException throwables) {
			response = "Internal error when linking.";
		}
		String finalResponse = response;
		message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
	}

	private static void linkLichess(String args, Server server, Message message, boolean isAdmin) {
		String[] parts = args.split(" ");
		String linking = parts[0];
		String user = message.getAuthor().get().getId().asString();
		String response = null;
		if (parts.length >= 2) {
			if (!isAdmin) {
				response = "You do not have permission to set other people's links.";
			} else {
				Matcher m = ID_PATTERN.matcher(parts[1]);
				if (m.matches()) {
					user = m.group(1);
				} else {
					response = "Second argument is malformed.";
				}
			}
			if (response != null) {
				String finalResponse = response;
				message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
				return;
			}
		}

		try {
			Player discord = DataManager.getPlayerByDiscordId(server.guildId, user);
			Player lichess = DataManager.getPlayerByLichessId(server.guildId, linking);
			if (discord.equals(lichess)) {
				response = "You've alrady linked those accounts.";
			} else if (discord.lichess != null) {
				response = String.format("You already have a lichess account linked. Use %s player to check your data.", server.prefix);
			} else if (lichess.discordId != null) {
				response = "Someone has already linked that lichess username.";
			} else if (!Lichess.checkAccount(linking)) {
				response = "That Lichess account does not exist.";
			}
			if (response == null) {
				Linker.linkLichessWithDiscord(lichess, discord);
				response = "Success.";
			}
		} catch (SQLException throwables) {
			response = "Internal error when linking.";
		}
		String finalResponse = response;
		message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
	}

}