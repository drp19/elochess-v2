package me.drpho.bots.elochess.events;

import discord4j.core.object.entity.Message;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import me.drpho.bots.elochess.DataManager;
import me.drpho.bots.elochess.EloCalculator;
import me.drpho.bots.elochess.Linker;
import me.drpho.bots.elochess.lichess.Lichess;
import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Mode;
import me.drpho.bots.elochess.models.Player;
import me.drpho.bots.elochess.models.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GameProcessor {

	private static final Pattern BASE_PATTERN = Pattern.compile("^(-m (?<mode>\\S+) +)?(?<white><@!?\\d{18}>|\\S+) +(?<black><@!?\\d{18}>|\\S+) +(?<win>\\k<white>|\\k<black>|white|black|draw)( +-(?<opt>[crsdt]))?( +/{0,2}(?<com>.*))?$");
	private static final Pattern ID_PATTERN = Pattern.compile("^<@!?(\\d{18})>$");
	private static final Pattern LICHESS_PATTERN = Pattern.compile("^(?>(?>https?://)?lichess\\.org/([A-Za-z0-9]+)/?)( +-m (?<mode>\\S+))?( +(?<white><@!?\\d{18}>|\\S+) +(?<black><@!?\\d{18}>|\\S+))?( +//(?<com>.*))?$");

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());


	public static void processCommand(String subCommand, String args, Server server, Message message) {
		try {
			boolean confirmMode = false, usingDefaultMode = false, warnName = false, linkedWhite = false,
					linkedBlack = false;

			ParseResult result = parseArgs(args);
			String modeStr = result.mode, timePattern = null;
			Player white = null, black = null;
			Game.Winner winner = result.winner;
			Game.EndType gameResult = result.endType;
			Instant timestamp = Instant.now();
			String link = null;

			if (result.type == Game.Type.MANUAL) {
				switch (result.whiteSource) {
					case DISCORD: white = DataManager.getPlayerByDiscordId(server.guildId, result.white); break;
					case NAME: white = DataManager.getPlayerByName(server.guildId, result.white); warnName = true; break;
				}
				switch (result.blackSource) {
					case DISCORD: black = DataManager.getPlayerByDiscordId(server.guildId, result.black); break;
					case NAME: black = DataManager.getPlayerByName(server.guildId, result.black); warnName = true; break;
				}
				//if no mode string was provided, use the server default and note that
				if (modeStr == null) {
					logger.debug("Using default mode");
					usingDefaultMode = true;
					modeStr = server.defaultMode;
				} else {
					Optional<String> parsed = Mode.parseModeString(modeStr);
					if (parsed.isPresent()) {
						timePattern = modeStr.replace("-","|").replace("+","|");
						logger.debug("Parsed mode to {}", modeStr);
						modeStr = parsed.get();
						confirmMode = true;
					}
				}
			} else if (result.type == Game.Type.LICHESS) {
				Lichess.LichessGame lichessGame = Lichess.getGame(result.lichessId);
				timestamp = Instant.ofEpochMilli(lichessGame.getTimestamp());
				link = "https://lichess.org/" + lichessGame.getId();
				// if no mode was provided, use the one from lichess and note it to the user
				if (modeStr == null) {
					modeStr = lichessGame.getMode();
					logger.debug("Using lichess mode {}", modeStr);
					confirmMode = true;
				} else {
					Optional<String> parsed = Mode.parseModeString(modeStr);
					if (parsed.isPresent()) {
						modeStr = parsed.get();
						logger.debug("Parsed mode to {}", modeStr);
						confirmMode = true;
					}
				}
				timePattern = lichessGame.getTimeStr();
				if (DataManager.gameExistsWithLink(link)) {
					throw ParseError.DUPLICATE_GAME;
				}
				white = DataManager.getPlayerByLichessId(server.guildId, lichessGame.getWhite());
				// if player was provided: if lichess + entrytype are already linked and not to each other, throw an
				// error. if linked to each other, ignore. Otherwise, link the two.
				if (result.white != null) {
					switch (result.whiteSource) {
						case DISCORD: {
							Player target = DataManager.getPlayerByDiscordId(white.guildId, result.white);
							if (white.equals(target)) {
								logger.debug("Link already exists for white");
								break;
							}
							if (white.discordId != null || target.lichess != null) {
								logger.debug("Discord or lichess is already defined for white");
								throw ParseError.OVERWRITE_LINK;
							}
							Player linked = Linker.linkLichessWithDiscord(white, target);
							if (linked != null) {
								white = linked;
							} else {
								throw ParseError.INTERNAL_ERROR;
							}
							linkedWhite = true;
						} break;
						case NAME: {
							Player target = DataManager.getPlayerByName(white.guildId, result.white);
							if (white.equals(target)) {
								logger.debug("Link already exists for white");
								break;
							}
							if (white.name != null || target.lichess != null) {
								logger.debug("Discord or lichess is already defined for white");
								throw ParseError.OVERWRITE_LINK;
							}
							Player linked = Linker.linkLichessWithName(white, target);
							if (linked != null) {
								white = linked;
							} else {
								throw ParseError.INTERNAL_ERROR;
							}
							linkedWhite = true;
							warnName = true;
						} break;
					}
				}
				black = DataManager.getPlayerByLichessId(server.guildId, lichessGame.getBlack());
				// if black player was provided and the link does not exist, link it
				if (result.black != null) {
					switch (result.blackSource) {
						case DISCORD: {
							Player target = DataManager.getPlayerByDiscordId(black.guildId, result.black);
							if (black.equals(target)) {
								logger.debug("Link already exists for black");
								break;
							}
							if (black.discordId != null || target.lichess != null) {
								logger.debug("Discord or lichess is already defined for black");
								throw ParseError.OVERWRITE_LINK;
							}
							Player linked = Linker.linkLichessWithDiscord(black, target);
							if (linked != null) {
								black = linked;
							} else {
								throw ParseError.INTERNAL_ERROR;
							}
							linkedBlack = true;
						} break;
						case NAME: {
							Player target = DataManager.getPlayerByName(black.guildId, result.black);
							if (black.equals(target)) {
								logger.debug("Link already exists for black");
								break;
							}
							if (black.name != null || target.lichess != null) {
								logger.debug("Discord or lichess is already defined for black");
								throw ParseError.OVERWRITE_LINK;
							}
							Player linked = Linker.linkLichessWithName(black, target);
							if (linked != null) {
								black = linked;
							} else {
								throw ParseError.INTERNAL_ERROR;
							}
							linkedBlack = true;
							warnName = true;
						} break;
					}
				}
				winner = lichessGame.getWinner();
				gameResult = lichessGame.getResult();
			} else {
				throw ParseError.PATTERN_ERROR;
			}

			Optional<Mode> mode = DataManager.getModeByName(server.guildId, modeStr);
			if (!mode.isPresent()) {
				throw ParseError.UNKNOWN_MODE;
			}
			Game newGame = new Game(server.guildId, message.getId().asString(), message.getAuthor().get().getId().asString(),
					Game.Status.ACTIVE, timestamp, mode.get(), timePattern, white.id, black.id, white.getElo(mode.get().name), black.getElo(mode.get().name),
					gameResult, winner, result.type, link, result.comment);
			EloCalculator.PlayerPair newPlayers = EloCalculator.calculateNewElo(new EloCalculator.PlayerPair(white, black), newGame);
			DataManager.addGame(newGame);
			DataManager.updatePlayerElo(newPlayers.white);
			DataManager.updatePlayerElo(newPlayers.black);

			String toSend = "";
			if (usingDefaultMode) {
				toSend += String.format("Using the default mode: *%s*\n", modeStr);
			}
			if (confirmMode) {
				toSend += String.format("Mode was parsed to *%s*. If this isn't what you wanted, delete this game (TBD) and specify the mode. %s modes list may be helpful.\n", modeStr, server.prefix);
			}
			if (linkedBlack) {
				toSend += "Link for black player successful.\n";
			}
			if (linkedWhite) {
				toSend += "Link for white player successful.\n";
			}
			if (warnName) {
				toSend += "**Warning:** Using names is discouraged. Make sure you entered the name correctly!\n";
			}
			toSend += String.format("Added game. Use %s top %s to see the updated leaderboard.", server.prefix, modeStr);
			String finalToSend = toSend;
			message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalToSend)).subscribe();

		} catch (SQLException throwables) {
			throwables.printStackTrace();
		} catch (ParseError e) {
			logger.warn(e.shortMessage);
			message.getChannel().flatMap(messageChannel -> messageChannel.createMessage("Could not add game: " + e.shortMessage)).subscribe();
		}
	}

	/**
	 * Parses a command into parts and validates that all required parts are present
	 * @return An object that contains mode, players, etc. None of these are validated to be correct, and only those present in the command are included.
	 * @throws ParseError If any parameter is unable to be processed.
	 */
	static ParseResult parseArgs(String args) throws ParseError {
		Matcher matcher = BASE_PATTERN.matcher(args);
		Matcher lichessMatcher = LICHESS_PATTERN.matcher(args);
		if (matcher.matches()) {
			// get mode
			Game.Type type = Game.Type.MANUAL;
			String mode = null;
			if (matcher.group("mode") != null) {
				mode = matcher.group("mode");
			}

			//get players
			String white = matcher.group("white");
			String black = matcher.group("black");
			Player.EntryType whiteSource, blackSource;

			Matcher whiteMatcher = ID_PATTERN.matcher(white);
			if (whiteMatcher.matches()) {
				white = whiteMatcher.group(1);
				whiteSource = Player.EntryType.DISCORD;
			} else {
				whiteSource = Player.EntryType.NAME;
			}
			Matcher blackMatcher = ID_PATTERN.matcher(black);
			if (blackMatcher.matches()) {
				black = blackMatcher.group(1);
				blackSource = Player.EntryType.DISCORD;
			} else {
				blackSource = Player.EntryType.NAME;
			}

			//get winner
			String winnerStr = matcher.group("win");
			Matcher winMatcher = ID_PATTERN.matcher(winnerStr);
			if (winMatcher.matches()) {
				winnerStr = winMatcher.group(1);
			}
			Game.Winner winnerObj;
			if (winnerStr.equalsIgnoreCase(white) || winnerStr.equalsIgnoreCase("white")) {
				winnerObj = Game.Winner.WHITE;
			} else if (winnerStr.equalsIgnoreCase(black) || winnerStr.equalsIgnoreCase("black")) {
				winnerObj = Game.Winner.BLACK;
			} else if (winnerStr.equalsIgnoreCase("draw")) {
				winnerObj = Game.Winner.NONE;
			} else {
				throw ParseError.UNKNOWN_WINNER;
			}

			//get opt
			Game.EndType endType;
			String opt = matcher.group("opt");
			if (opt == null) {
				if (winnerObj == Game.Winner.NONE) {
					opt = "d";
				} else {
					opt = "c";
				}
			}
			switch (opt) {
				case "c": endType = Game.EndType.MATE; break;
				case "r": endType = Game.EndType.RESIGN; break;
				case "s": endType = Game.EndType.STALEMATE; break;
				case "d": endType = Game.EndType.DRAW; break;
				case "t": endType = Game.EndType.TIME; break;
				default: throw GameProcessor.ParseError.UNKNOWN_OPT;
			}
			return new ParseResult(type, mode, white, black, whiteSource, blackSource, winnerObj, endType, null, matcher.group("com"));
		} else if (lichessMatcher.matches()) {
			Game.Type type = Game.Type.LICHESS;
			String gameId = lichessMatcher.group(1);
			String mode = lichessMatcher.group("mode");
			String white = lichessMatcher.group("white");
			String black = lichessMatcher.group("black");
			Player.EntryType whiteSource, blackSource;

			if (white != null && black != null) {
				Matcher whiteMatcher = ID_PATTERN.matcher(white);
				if (whiteMatcher.matches()) {
					white = whiteMatcher.group(1);
					whiteSource = Player.EntryType.DISCORD;
				} else {
					whiteSource = Player.EntryType.NAME;
				}
				Matcher blackMatcher = ID_PATTERN.matcher(black);
				if (blackMatcher.matches()) {
					black = blackMatcher.group(1);
					blackSource = Player.EntryType.DISCORD;
				} else {
					blackSource = Player.EntryType.NAME;
				}
			} else {
				whiteSource = Player.EntryType.LICHESS;
				blackSource = Player.EntryType.LICHESS;
			}

			return new ParseResult(type, mode, white, black, whiteSource, blackSource, null, null, gameId, lichessMatcher.group("com"));
		}
		else {
			throw ParseError.PATTERN_ERROR;
		}
	}

	@AllArgsConstructor
	@Getter
	static class ParseResult {
		@NonNull private final Game.Type type;
		private final String mode;
		private final String white, black;
		@NonNull private final Player.EntryType whiteSource, blackSource;
		private final Game.Winner winner;
		private final Game.EndType endType;
		private final String lichessId;
		private final String comment;
	}

	public static class ParseError extends Throwable {
		public static final ParseError UNKNOWN_WINNER = new ParseError("Could not determine winner");
		public static final ParseError UNKNOWN_OPT = new ParseError("Could not parse option");
		public static final ParseError LICHESS_NOT_FINISHED = new ParseError("Lichess game is not finished");
		public static final ParseError LICHESS_ERROR = new ParseError("Error retrieving game from Lichess");
		public static final ParseError PATTERN_ERROR = new ParseError("Message is not in the correct format. See the help text.");
		public static final ParseError INTERNAL_ERROR = new ParseError("Internal error");
		public static final ParseError UNKNOWN_MODE = new ParseError("Unknown mode code");
		public static final ParseError DUPLICATE_GAME = new ParseError("Duplicate game code");
		public static final ParseError OVERWRITE_LINK = new ParseError("Trying to redefine linked account");


		private final String shortMessage;

		private ParseError(String shortMessage) {
			this.shortMessage = shortMessage;
		}
	}

}
