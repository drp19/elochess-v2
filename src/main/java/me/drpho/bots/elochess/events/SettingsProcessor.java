package me.drpho.bots.elochess.events;

import discord4j.core.object.entity.Message;
import me.drpho.bots.elochess.DataManager;
import me.drpho.bots.elochess.EmbedCreator;
import me.drpho.bots.elochess.models.Mode;
import me.drpho.bots.elochess.models.Player;
import me.drpho.bots.elochess.models.Server;

import java.sql.SQLException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SettingsProcessor {

	private static final Pattern ADD_PATTERN = Pattern.compile("add (\\S{0,12})( -u)?");
	private static final Pattern ID_PATTERN = Pattern.compile("^<@!?(\\d{18})>$");

	public static void processCommand(String subCommand, String args, Server server, Message message, boolean isAdmin) {
		switch (subCommand) {
			case "settings": parseSettings(args); break;
			case "modes": parseModes(args, server, message, isAdmin); break;
			case "player": parsePlayer(args, server, message); break;
		}
	}

	private static void parseSettings(String args) {}

	private static void parsePlayer(String args, Server server, Message message) {
		String target = message.getAuthor().get().getId().asString();
		String response;
		if (!args.equals("")) {
			Matcher m = ID_PATTERN.matcher(args);
			if (m.matches()) {
				target = m.group(1);
			} else {
				response = "Argument is malformed. It should be a discord mention.";
				String finalResponse1 = response;
				message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse1)).subscribe();
				return;
			}
		}
		try {
			Player p = DataManager.getPlayerByDiscordId(server.guildId, target);
			message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(embed -> EmbedCreator.printPlayer(embed, p, message.getGuild())))).subscribe();
			return;
		} catch (SQLException throwables) {
			response = "Internal error when retrieving data.";
		}
		String finalResponse = response;
		message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
	}


	private static void parseModes(String args, Server server, Message message, boolean isAdmin) {
		List<Mode> modes = DataManager.getModes(server.guildId);
		if (args.equals("list") || args.equals("")) {
			message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(embed -> EmbedCreator.printModes(embed, modes)))).subscribe();
			return;
		}
		String response = "";
		Matcher addMatcher = ADD_PATTERN.matcher(args);
		if (addMatcher.matches()) {
			String adding = addMatcher.group(1);
			boolean rated = addMatcher.group(2) == null;
			if (!(server.allowAddMode || isAdmin)) {
				response = "This server does not allow you to add modes.";
			} else if (modes.stream().anyMatch(m -> m.name.equals(adding))) {
				response = "That mode already exists.";
			} else {
				Mode newMode = new Mode(server.guildId, adding, null, rated);
				try {
					DataManager.addMode(newMode);
					response = "Mode added.";
				} catch (SQLException throwables) {
					response = "Internal error when adding mode.";
				}
			}
		} else {
			response = String.format("Unknown command. Use %s help modes", server.prefix);
		}
		String finalResponse = response;
		message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
	}


}