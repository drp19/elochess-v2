package me.drpho.bots.elochess.events;

import discord4j.core.object.entity.Message;
import lombok.NonNull;
import me.drpho.bots.elochess.DataManager;
import me.drpho.bots.elochess.EmbedCreator;
import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Mode;
import me.drpho.bots.elochess.models.Player;
import me.drpho.bots.elochess.models.Server;

import java.sql.SQLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class HistoryProcessor {
	private static final Pattern GAMES_PATTERN = Pattern.compile("^(-m (?<mode>\\S+))?( *(?<p1><@!?\\d{18}>|\\S+))?( *(?<p2><@!?\\d{18}>|\\S+))?$");
	private static final Pattern ID_PATTERN = Pattern.compile("^<@!?(\\d{18})>$");

	public static void processCommand(String subCommand, String args, Server server, Message message) {
		switch (subCommand) {
			case "games": processGames(args, server, message); break;
			case "game": processGame(args, server, message); break;
			case "top": processTop(args, server, message); break;
		}
	}

	private static void processTop(String args, Server server, Message message) {
		String m;
		boolean warnDefault = false;
		if (args.equals("")) {
			m = server.defaultMode;
		} else {
			Optional<Mode> res = DataManager.getModeByName(server.guildId, args);
			if (res.isPresent()) {
				m = res.get().name;
			} else {
				m = server.defaultMode;
				warnDefault = true;
			}
		}
		List<Player> players = DataManager.getAllPlayers(server.guildId);
		Map<Player, Double> elo = players.stream().filter(player -> player.getElo(m) != Player.DEFAULT_ELO)
				.collect(Collectors.toMap(entry -> entry, p -> p.getElo(m)));
		elo = sortByValue(elo);
		Map<Player, Double> finalElo = elo;
		boolean finalWarnDefault = warnDefault;
		message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(embed -> EmbedCreator.printTop(embed, m, finalWarnDefault, finalElo, message.getGuild())))).subscribe();
	}

	private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
		list.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

	private static void processGame(String args, Server server, Message message) {
		String[] parts = args.split(" ");
		int id = 0;
		String response = "";
		try {
			id = Integer.parseInt(parts[0]);
		} catch (NumberFormatException e) {
			response = "Provide a game ID as an argument";
		}
		if (parts.length >= 2 && parts[1].equals("delete")) {
			//TODO: Delete
		} else {
			Game g = DataManager.getGame(id);
			if (g != null) {
				message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(embed -> EmbedCreator.printGame(embed, g, message.getGuild())))).subscribe();
				return;
			} else {
				response = "That ID does not exist.";
			}
		}
		String finalResponse = response;
		message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(finalResponse)).subscribe();
	}

	private static void processGames(String args, Server server, Message message) {
		Matcher matcher = GAMES_PATTERN.matcher(args);
		if (matcher.matches()) {
			try {
				String mode = matcher.group("mode");
				String p1Str = matcher.group("p1"), p2Str = matcher.group("p2");
				Player p1 = null, p2 = null;
				if (p1Str != null) {
					Matcher p1Matcher = ID_PATTERN.matcher(p1Str);
					if (p1Matcher.matches()) {
						p1Str = p1Matcher.group(1);
						p1 = DataManager.getPlayerByDiscordId(server.guildId, p1Str);
					} else {
						p1 = DataManager.getPlayerByName(server.guildId, p1Str);
					}
				}
				if (p2Str != null) {
					Matcher p2Matcher = ID_PATTERN.matcher(p2Str);
					if (p2Matcher.matches()) {
						p2Str = p2Matcher.group(1);
						p2 = DataManager.getPlayerByDiscordId(server.guildId, p2Str);
					} else {
						p2 = DataManager.getPlayerByName(server.guildId, p2Str);
					}
				}
				List<Game> games = DataManager.getAllGames(server.guildId);
				Player finalP1 = p1;
				Player finalP2 = p2;
				games = games.stream().filter(g -> mode == null || g.getMode().name.equals(mode))
						.filter(g -> finalP1 == null || g.getWhite() == finalP1.id || g.getBlack() == finalP1.id)
						.filter(g -> finalP2 == null || g.getWhite() == finalP2.id || g.getBlack() == finalP2.id)
						.sorted(Comparator.comparing(Game::getTimestamp)).collect(Collectors.toList());
				List<String> filters = new ArrayList<>();
				if (mode != null) {
					filters.add("mode = " + mode);
				}
				if (p1 != null) {
					filters.add("player = " + p1.getPrint(message.getGuild()));
				}
				if (p2 != null) {
					filters.add("player = " + p2.getPrint(message.getGuild()));
				}
				String filterStr = String.join(", ", filters);
				Map<Integer, String> players = DataManager.getAllPlayerString(server.guildId, message.getGuild());
				List<Game> finalGames = games;
				message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setContent(EmbedCreator.printGames(finalGames, players, filterStr, mode == null, server.prefix)))).subscribe();
			} catch (SQLException throwables) {
				throwables.printStackTrace();
			}
		} else {
			message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(String.format("Could not process message. %s help games may be useful.", server.prefix))).subscribe();
		}

	}

}
