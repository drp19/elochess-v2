package me.drpho.bots.elochess.events;

import discord4j.core.object.entity.Message;
import me.drpho.bots.elochess.EmbedCreator;
import me.drpho.bots.elochess.models.Server;

public class HelpProcessor {

	public static void processCommand(String subCommand, String args, Server server, Message message) {
		switch (args) {
			case "": case "help": message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(embed -> EmbedCreator.generalHelp(embed, server.prefix)))).subscribe(); break;
			case "play": message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(embed -> EmbedCreator.playHelp(embed, server.defaultMode)))).subscribe(); break;
			case "modes": case "lichess": case "name":  case "game": case "top":message.getChannel().flatMap(messageChannel -> messageChannel.createMessage("No further help is available for this command.")).subscribe(); break;
			case "settings": break;
			case "games": message.getChannel().flatMap(messageChannel -> messageChannel.createMessage(spec -> spec.setEmbed(EmbedCreator::gamesHelp))).subscribe(); break;
		}
	}
}
