package me.drpho.bots.elochess;

import discord4j.core.object.entity.Guild;
import discord4j.core.spec.EmbedCreateSpec;
import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Mode;
import me.drpho.bots.elochess.models.Player;
import reactor.core.publisher.Mono;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

public class EmbedCreator {

	public static EmbedCreateSpec printModes(EmbedCreateSpec spec, List<Mode> modes) {
		modes.sort(Comparator.comparing(m -> m.name));
		StringBuilder name = new StringBuilder(), desc = new StringBuilder(), unrated = new StringBuilder();
		for (Mode m : modes) {
			name.append(m.name).append("\n");
			desc.append(Objects.requireNonNullElse(m.description, "-")).append("\n");
			unrated.append(m.rated ? "rated" : "-").append("\n");
		}
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle("Server Modes");
		spec.addField("Name", name.toString(), true);
		spec.addField("Description", desc.toString(), true);
		spec.addField("Is rated?", unrated.toString(), true);
		spec.setFooter("Created by Drew", null);
		return spec;
	}

	public static EmbedCreateSpec printGame(EmbedCreateSpec spec, Game g, Mono<Guild> guild) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone( ZoneId.systemDefault());
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle(String.format("Game %s at %s", g.getId(), formatter.format(g.getTimestamp())));
		spec.addField("Mode", g.getMode().name, true);
		spec.addField("Game Time", Objects.requireNonNullElse(g.getTimeStr(), "-"), true);
		spec.addField("White Player (ELO)", Objects.requireNonNull(DataManager.getPlayerString(guild, g.getWhite())) + String.format(" (%.1f)", g.getWhitePriorElo()), true);
		spec.addField("Black Player (ELO)", Objects.requireNonNull(DataManager.getPlayerString(guild, g.getBlack())) + String.format(" (%.1f)", g.getWhitePriorElo()), true);
		spec.addField("Winner", g.getWinner().print, true);
		spec.addField("Ending", g.getEndType().print, true);
		spec.addField("Link", Objects.requireNonNullElse(g.getLink(), "N/A"), true);
		spec.addField("Comments", Objects.requireNonNullElse(g.getComment(), "-"), false);
		spec.setFooter("Created by Drew", null);
		return spec;
	}

	public static EmbedCreateSpec printPlayer(EmbedCreateSpec spec, Player p, Mono<Guild> guild) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm").withZone( ZoneId.systemDefault());
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle("Player Summary");
		spec.addField("Discord Name", p.getPrint(guild), true);
		spec.addField("Lichess Username", Objects.requireNonNullElse(p.lichess, "n/a"), true);
		spec.addField("Name", Objects.requireNonNullElse(p.name, "n/a"), true);
		spec.addField("Games Played", DataManager.gamesPlayed(p.id)+"", true);
		String max;
		try {
			max = String.format("%.1f", Collections.max(p.getEloMap().values()));
		} catch (NoSuchElementException ignored) {
			max = "n/a";
		}
		spec.addField("Highest ELO", max, true);
		spec.setFooter("Created by Drew", null);
		return spec;
	}

	public static EmbedCreateSpec printTop(EmbedCreateSpec spec, String mode,  boolean warn, Map<Player, Double> players, Mono<Guild> guild) {
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle(String.format("Leaderboard for %s", mode));
		if (players.isEmpty()) {
			spec.setDescription("No players in this mode.");
		} else {
			if (warn) {
				spec.setDescription("Warning: provided mode not found, using default.");
			}
			StringBuilder name = new StringBuilder(), elo = new StringBuilder();
			for (Map.Entry<Player, Double> entry : players.entrySet()) {
				name.append(entry.getKey().getPrint(guild)).append("\n");
				elo.append(String.format("%.1f\n", entry.getValue()));
			}
			spec.addField("Name", name.toString(), true);
			spec.addField("ELO", elo.toString(), true);
		}
		spec.setFooter("Created by Drew", null);
		return spec;
	}


	public static String printGames(List<Game> games, Map<Integer, String> playerPrint, String filterString, boolean filterMode, String prefix) {
		StringBuilder ret = new StringBuilder();
		if (games.size() == 0) {
			return "No games selected.";
		}
		StringBuilder data = new StringBuilder();
		DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE.withZone( ZoneId.systemDefault());
		int maxWhite = 5, maxBlack = 5, maxMode = 5;
		for (Game g : games) {
			if (maxWhite < playerPrint.get(g.getWhite()).length()) {
				maxWhite = playerPrint.get(g.getWhite()).length();
			}
			if (maxBlack < playerPrint.get(g.getBlack()).length()) {
				maxBlack = playerPrint.get(g.getBlack()).length();
			}
			if (maxMode < g.getMode().name.length()) {
				maxMode = g.getMode().name.length();
			}
		}
		if (maxWhite > 16) maxWhite = 16;
		if (maxBlack > 16) maxBlack = 16;
		if (maxMode > 12) maxMode = 12;
		if (filterMode) {
			data.append(String.format("%-5s%-13s%-" + (maxMode+3) + "s%-" + (maxWhite+3) + "s%-" + (maxBlack+3) + "s%-9s%-9s%-5s\n",
					"ID", "Time", "Mode", "White", "Black", "W ELO", "B ELO", "Winner"));
		} else {
			data.append(String.format("%-5s%-13s%-" + (maxWhite+3) + "s%-" + (maxBlack+3) + "s%-9s%-9s%-5s\n",
					"ID", "Time", "White", "Black", "W ELO", "B ELO", "Winner"));
		}

		for (Game g : games) {
			String dataString;
			if (filterMode) {
				dataString = String.format("%-5s%-13s%-" + (maxMode+3) + "s%-" + (maxWhite+3) + "s%-" + (maxBlack+3) + "s%-9s%-9s%-5s\n", g.getId(), formatter.format(g.getTimestamp()), g.getMode().name,
						playerPrint.get(g.getWhite()).substring(0, Math.min(playerPrint.get(g.getWhite()).length(), maxWhite)),
						playerPrint.get(g.getBlack()).substring(0, Math.min(playerPrint.get(g.getBlack()).length(), maxBlack)),
						String.format("%.1f", g.getWhitePriorElo()), String.format("%.1f", g.getBlackPriorElo()),
						g.getWinner().print);
			} else {
				dataString = String.format("%-5s%-13s%-" + (maxWhite+3) + "s%-" + (maxBlack+3) + "s%-9s%-9s%-5s\n", g.getId(), formatter.format(g.getTimestamp()),
						playerPrint.get(g.getWhite()).substring(0, Math.min(playerPrint.get(g.getWhite()).length(), maxWhite)),
						playerPrint.get(g.getBlack()).substring(0, Math.min(playerPrint.get(g.getBlack()).length(), maxBlack)),
						String.format("%.1f", g.getWhitePriorElo()), String.format("%.1f", g.getBlackPriorElo()),
						g.getWinner().print);
			}

			data.append(dataString);
		}
		ret.append("**Games**\n");
		ret.append(filterString.equals("") ? "Not filtered." : "Filtered by " + filterString).append("\n");
		ret.append("```").append(data.toString()).append("```");
		ret.append(String.format("Use %s game <id> to get more detailed information.", prefix));
		return ret.toString();
	}

	public static EmbedCreateSpec generalHelp(EmbedCreateSpec spec, String prefix) {
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle("Help menu");
		spec.setDescription("This bot keeps track of ELO in this server. Different modes are available,\n" +
				"and follow the standard chess times. It will track games \n" +
				"with your discord account, or you can link your Lichess profile. You can also set \n" +
				"a name, but this is discouraged. If you want to use names, you should link as soon as possible (see below)\n" +
				"All commands are prefixed with " + prefix + "\n" +
				"Get help with any commands that have (H) by using " + prefix + " help <command>");
		spec.addField("Warning: All linking commands are permanent!", "Be careful.", false);
		spec.addField("Commands", "play: (H) enter a game into the system (see help for options)\n" +
				"help <command>: print this menu or a submenu for a command\n" +
				"games: (H) list games played with optional filters\n" +
				"game <id>: get more detailed info about a game (coming soon: deleting)\n" +
				"top <mode>: get the scoreboard for the specified mode\n" +
				"lichess <username>: link your lichess account to your discord\n" +
				"name <name>: set a name to be referred by\n" +
				"modes [add <mode>]: list the modes on the server, or add another one\n" +
				"player <mention>: get player info for the specified player, or leave it blank for your own info.", false);
		spec.setFooter("Created by Drew", null);
		return spec;
	}

	public static EmbedCreateSpec playHelp(EmbedCreateSpec spec, String defaultMode) {
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle("Play command help");
		spec.setDescription("Games can be inputted manually or via Lichess. ELO will automatically be updated\n" +
				"unless it is for an unranked mode. If there's something wrong with the command, it'll let you know.");
		spec.addField("Warning: All linking commands are permanent!", "Be careful.", false);
		spec.addField("Manual Syntax: play [-m mode] white black winner [-option] //comments",
				"mode: specify a mode for the game - default is " + defaultMode + "\n" +
						"white & black: the players; either mention the player or provide their name (discouraged)\n" +
						"winner: either 'white', 'black', 'draw', or one of the two player options earlier in the message\n" +
						"option: optionally, you can provide how the game ended: 'c' - checkmate, 'r' - resign, 't' - out of time,\n" +
						"  'd' - draw, or 's' stalemate. the first 3 are valid if a player won, the other two valid if it was a draw\n" +
						"comments: provide comments about the game if you want", false);
		spec.addField("Lichess Syntax: play [url] [-m mode] [white black] //comments",
				"url: the lichess URL for the game\n" +
						"mode: specify a mode for the game - this isn't necessary most of the time, since it will pull from lichess.\n" +
						"white & black: these can be provided to link the white and black players from Lichess with either the mention of the player or their name (discouraged)\n" +
						"BE CAREFUL! Linking is (basically) permanent, so don't mess it up!\n" +
						"comments: provide comments about the game if you want", false);
		spec.setFooter("Created by Drew", null);
		return spec;
	}

	public static EmbedCreateSpec gamesHelp(EmbedCreateSpec spec) {
		spec.setAuthor("ChessBot", null, null);
		spec.setTitle("Games command help");
		spec.setDescription("List out games, with filters. Only the most recent 30 games are shown because\n" +
				"of character limits.");
		spec.addField("Syntax: games [-m mode] p1 p2",
				"mode: specify a mode to filter by. if the mode doesn't exist, this is ignored.\n" +
						"p1 & p2: the players; filter by one or both players in a game (order doesn't matter).\n" +
						"Either mention the player or provide their name", false);
		spec.setFooter("Created by Drew", null);
		return spec;
	}

}
