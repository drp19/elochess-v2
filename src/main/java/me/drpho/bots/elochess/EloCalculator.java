package me.drpho.bots.elochess;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Player;

public class EloCalculator {
	private static final int K = 30;

	public static PlayerPair calculateNewElo(PlayerPair players, Game game) {
		if (!game.getMode().rated) {
			return players;
		}
		double whiteCurrent = players.white.getElo(game.getMode().name), blackCurrent = players.black.getElo(game.getMode().name);
		double whiteProb = expected(whiteCurrent, blackCurrent), blackProb = expected(blackCurrent, whiteCurrent);
		double whiteResult = 0, blackResult = 0;
		switch (game.getWinner()) {
			case WHITE: {
				whiteResult = 1.0;
				blackResult = 0.0;
			} break;
			case BLACK: {
				whiteResult = 0.0;
				blackResult = 1.0;
			} break;
			case NONE: {
				whiteResult = blackResult = 0.5;
			}
		}
		Player whiteNew = players.white.withNewElo(game.getMode().name,whiteCurrent + K*(whiteResult - whiteProb));
		Player blackNew = players.black.withNewElo(game.getMode().name,blackCurrent + K*(blackResult - blackProb));
		return new PlayerPair(whiteNew, blackNew);
	}

	/**
	 * Part of the ELO calculation.
	 * @param rating1 player 1 ELO
	 * @param rating2 player 2 ELO
	 * @return the expected win probability of player 1
	 */
	private static double expected(double rating1, double rating2) {
		return 1.0 / (1.0 + (Math.pow(10, (rating2 - rating1) / 400)));
	}

	@AllArgsConstructor
	@EqualsAndHashCode
	public static final class PlayerPair {
		public final Player white, black;
	}
}
