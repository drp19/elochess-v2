package me.drpho.bots.elochess;

import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.guild.GuildCreateEvent;
import discord4j.core.event.domain.guild.MemberJoinEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.User;
import discord4j.core.object.reaction.Reaction;
import discord4j.rest.util.Permission;
import me.drpho.bots.elochess.events.EventHandler;
import me.drpho.bots.elochess.models.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Bot {
	public static final String PERMS = "469838917"; //TODO: Update
	public static final String DB_NAME = "bot.db";

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	public static GatewayDiscordClient CLIENT;

	public static void main (String [] args) {
		if (args.length != 1) {
			logger.error("Must have one parameter with the bot token.");
			System.exit(1);
		}

		try {
			DataManager.connect(DB_NAME);
		} catch (SQLException ignored) {
			logger.error("Could not connect to SQL Database. Exiting.");
			System.exit(1);
		}
		final DiscordClient client = DiscordClient.create(args[0]);
		final GatewayDiscordClient gateway = client.login().block();

		registerEvents(gateway);
		logger.info("Setup completed.");
		CLIENT = gateway;
		gateway.onDisconnect().block();
	}

	private static void registerEvents(GatewayDiscordClient client) {
		client.getEventDispatcher().on(ReadyEvent.class)
				.subscribe(event -> {
					User self = event.getSelf();
					logger.info("Logged in as {}#{}", self.getUsername(), self.getDiscriminator());
					logger.info("\n\n\n\n\n\nUse the following link to invite the bot to your server:");
					logger.info("https://discordapp.com/oauth2/authorize?client_id=" + client.getSelfId().asString() + "&scope=bot&permissions=" + PERMS);
					logger.info("If you don't allow all the perms, the bot probably won't work right.\n\n\n\n\n");
				});
		client.getEventDispatcher().on(GuildCreateEvent.class)
				.subscribe(event -> {
					try {
						DataManager.joinServer(event.getGuild().getId().asString());
						logger.info("Joined guild {}!", event.getGuild().getName());
					} catch (SQLException ignored) {
						logger.warn("SQL exception when joining server {}!", event.getGuild().getName());
					}
				});
		client.getEventDispatcher().on(MessageCreateEvent.class)
				.filter(event -> event.getGuildId().isPresent())
				.filter(event -> event.getMember().isPresent())
				.subscribe(event -> {
					Server server = DataManager.getServer(event.getGuildId().get().asString());
					if (server == null) {
						logger.warn("Could not process message");
						return;
					}
					boolean isAdmin = event.getMember().get().getBasePermissions().block().contains(Permission.MANAGE_GUILD);
					if (!(server.allowAllChannels || server.channels.contains(event.getMessage().getChannelId().asString()) || isAdmin)) {
						return;
					}
					String content = event.getMessage().getContent().trim();
					if (content.toLowerCase().startsWith(server.prefix) && content.split(" ").length >= 2) {
						EventHandler.process(content.split(" ", 2)[1], server, event.getMessage(), isAdmin);
					}
				});
		client.getEventDispatcher().on(ReactionAddEvent.class)
				.filter(event -> event.getGuildId().isPresent())
				.subscribe(event -> {
//					try {
//						Server server = DataManager.getServer(event.getGuildId().get().asString());
//						if (server == null) {
//							throw new IllegalStateException("Accessing non-existent server!");
//						}
//						Ban ban = DataManager.getBanByVoteMessageId(event.getMessageId().asString());
//						if (ban == null || !ban.getBanType().isProcessVote()
//								|| !EventHandler.getFormattedEmojiString(event.getEmoji()).equals(server.getEmoji())) {
//							return;
//						}
//						Optional<Integer> numReacts = event.getMessage().map(message -> message.getReactions().stream()
//								.filter(reaction -> reaction.getEmoji().equals(event.getEmoji())).map(Reaction::getCount).findFirst())
//								.block();
//						if (!numReacts.isPresent()) {
//							throw new IllegalStateException("Could not get the number of reactions - really odd.");
//						}
//						logger.info("Vote for {} now has {} votes.", ban.getTargetId(), numReacts.get());
//						EventHandler.processReaction(server, ban, numReacts.get(), event.getGuild().map(Guild::getName));
//					} catch (SQLException ignored) {
//						logger.warn("SQL exception when processing reaction {}!", event.getMessageId().asString());
//					} catch (IllegalStateException e) {
//						logger.warn("Could not process reaction: {}", e.getMessage());
//					}
				});

	}
}
