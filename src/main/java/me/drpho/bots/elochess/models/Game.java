package me.drpho.bots.elochess.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

@AllArgsConstructor
@Getter
public class Game {
	private final Integer id;
	private final String guildId;
	private final Instant timestamp;
	private final String createMessage, authorId;
	private final Status status;
	private final Mode mode;
	private final String timeStr;
	private final int white, black;
	private final Double whitePriorElo, blackPriorElo;
	private final Winner winner;
	private final EndType endType;
	private final Type type;
	private final String link, comment;

	public Game(String guildId, String createMessage, String authorId, Status status, Instant timestamp, Mode mode, String timeStr, int white, int black, double whitePriorElo, double blackPriorElo, EndType endType, Winner winner, Type type, String link, String comment) {
		this.id = null;
		this.guildId = guildId;
		this.timestamp = timestamp;
		this.createMessage = createMessage;
		this.authorId = authorId;
		this.status = status;
		this.mode = mode;
		this.timeStr = timeStr;
		this.white = white;
		this.black = black;
		this.whitePriorElo = whitePriorElo;
		this.blackPriorElo = blackPriorElo;
		this.endType = endType;
		this.winner = winner;
		this.type = type;
		this.link = link;
		this.comment = comment;
	}

	@AllArgsConstructor
	public enum EndType {
		MATE(true, 0, "Checkmate"),
		RESIGN(true, 1, "Resign"),
		TIME(true, 2, "Out of time"),
		STALEMATE(false, 3, "Stalemate"),
		DRAW(false, 4, "Draw");

		private final boolean hasWinner;
		public int id;
		public final String print;

		public static Optional<EndType> valueOf(int value) {
			return Arrays.stream(values())
					.filter(legNo -> legNo.id == value)
					.findFirst();
		}
	}

	@AllArgsConstructor
	public enum Winner {
		WHITE(0, "White"), BLACK(1, "Black"), NONE(2, "Draw");

		public final int id;
		public final String print;

		public static Optional<Winner> valueOf(int value) {
			return Arrays.stream(values())
					.filter(legNo -> legNo.id == value)
					.findFirst();
		}
	}

	@AllArgsConstructor
	public enum Type {
		MANUAL(0), LICHESS(1);

		public int id;

		public static Optional<Type> valueOf(int value) {
			return Arrays.stream(values())
					.filter(legNo -> legNo.id == value)
					.findFirst();
		}
	}

	@AllArgsConstructor
	public enum Status {
		ACTIVE(0), DELETED(1), PENDING(2);

		public int id;

		public static Optional<Status> valueOf(int value) {
			return Arrays.stream(values())
					.filter(legNo -> legNo.id == value)
					.findFirst();
		}
	}
}
