package me.drpho.bots.elochess.models;

import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.Guild;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public final class Player {
	public static final double DEFAULT_ELO = 1200;

	@EqualsAndHashCode.Include public final Integer id;
	public final String guildId, discordId, name, lichess, chessgame;
	private final Map<String, Double> elo;

	@NonNull
	public Double getElo(String mode) {
		return elo.getOrDefault(mode, DEFAULT_ELO);
	}

	public Player withNewElo(String mode, double elo) {
		Map<String, Double> newElo = new HashMap<>(this.elo);
		newElo.put(mode, elo);
		return new Player(id, guildId, discordId, name, lichess, chessgame, newElo);
	}

	public enum EntryType {
		NAME, DISCORD, LICHESS, CHESSMATCH
	}

	public Map<String, Double> getEloMap() {
		return new HashMap<>(elo);
	}

	public String getPrint(Mono<Guild> g) {
		if (discordId != null) {
			return g.flatMap(guild -> guild.getMemberById(Snowflake.of(discordId))).block().getDisplayName();
		}
		if (name != null) {
			return name;
		}
		if (lichess != null) {
			return "li:"+lichess;
		}
		return "";
	}
}
