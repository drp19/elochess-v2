package me.drpho.bots.elochess.models;

import lombok.AllArgsConstructor;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
public class Mode {
	public static final Mode BULLET = new Mode(null, "Bullet", "Games less than 3minutes.", true);
	public static final Mode BLITZ = new Mode(null, "Blitz", "Games between 3 and 10 minutes.", true);
	public static final Mode RAPID = new Mode(null, "Rapid", "Games between 10 and 60 minutes.", true);
	public static final Mode UNTIMED = new Mode(null, "Untimed", "Untimed games.", true);
	public static final Mode CASUAL = new Mode(null, "Casual", "Casual games with no ELO.", false);


	public final String guildId;
	public final String name, description;
	public final boolean rated;

	/**
	 * Takes in a mode string from the user and determines what general mode it falls under
	 * @param input The mode string
	 * @return The formatted string if it was changed, otherwise empty
	 */
	public static Optional<String> parseModeString(String input) {
		Pattern timePattern = Pattern.compile("^(\\d+)([-+|]\\d+)?$");
		Matcher matcher = timePattern.matcher(input);
		if (matcher.matches()) {
			int duration = Integer.parseInt(matcher.group(1));
			if (duration < 3) {
				return Optional.of("bullet");
			} else if (duration < 10) {
				return Optional.of("blitz");
			} else if (duration < 60) {
				return Optional.of("rapid");
			} else {
				return Optional.of("untimed");
			}
		} else {
			return Optional.empty();
		}
	}
}
