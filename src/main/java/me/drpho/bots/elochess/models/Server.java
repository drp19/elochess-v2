package me.drpho.bots.elochess.models;

import lombok.AllArgsConstructor;

import java.util.List;

@AllArgsConstructor
public class Server {
	public final String guildId;
	public final boolean allowDelete;
	public final boolean allowAddMode;
	public final boolean allowAllChannels;
	public final List<String> channels;
	public final String defaultMode;

	public final String prefix;
}
