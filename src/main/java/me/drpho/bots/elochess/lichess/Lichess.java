package me.drpho.bots.elochess.lichess;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import me.drpho.bots.elochess.DataManager;
import me.drpho.bots.elochess.events.GameProcessor;
import me.drpho.bots.elochess.models.Game;
import me.drpho.bots.elochess.models.Mode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;

/**
 * @author Drew Pleat
 * @version 1.2 03/22/20
 * Static methods for accessing the lichess API. Currently checks accounts and downloads games.
 * @since 1.2
 */
public final class Lichess {
	/**
	 * Checks to see if a username is a valid account on Lichess
	 *
	 * @param userName a lichess username
	 * @return True if the account is valid, false if it is not or the request fails otherwise.
	 */
	public static boolean checkAccount(String userName) {
		try {
			JSONArray arr = new JSONArray(readJsonFromUrl("https://lichess.org/api/users/status?ids=" + userName));
			return arr.length() != 0;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * Gets data from a game on Lichess
	 *
	 * @param id The lichess game ID
	 * @return Returns null if the game could not be parsed.
	 */
	public static LichessGame getGame(String id) throws GameProcessor.ParseError {
		try {
			id = id.substring(0, Math.min(id.length(), 8));
			JSONObject object = new JSONObject(readJsonFromUrl("https://lichess.org/game/export/" + id));

			//process status
			String status = object.getString("status");
			Game.EndType endType;
			switch (status) {
				case "mate": endType = Game.EndType.MATE; break;
				case "resign": endType = Game.EndType.RESIGN; break;
				case "stalemate": endType = Game.EndType.STALEMATE; break;
				case "draw": endType = Game.EndType.DRAW; break;
				case "outoftime": endType = Game.EndType.TIME; break;
				case "created": case "started": throw GameProcessor.ParseError.LICHESS_NOT_FINISHED;
				default: throw GameProcessor.ParseError.UNKNOWN_OPT;
			}

			Game.Winner winner = Game.Winner.NONE;
			if (object.has("winner")) {
				String winnerStr = object.getString("winner");
				switch (winnerStr) {
					case "white": winner = Game.Winner.WHITE; break;
					case "black": winner = Game.Winner.BLACK; break;
					default: throw GameProcessor.ParseError.UNKNOWN_WINNER;
				}
			}

			//process time, players
			long timestamp = object.getLong("lastMoveAt");
			JSONObject players = object.getJSONObject("players");
			String white = players.getJSONObject("white").getJSONObject("user").getString("id");
			String black = players.getJSONObject("black").getJSONObject("user").getString("id");

			//process mode
			String time = null;
			if (object.has("clock")){
				JSONObject clock = object.getJSONObject("clock");
				int start = clock.getInt("initial"), inc = clock.getInt("increment");
				time = start / 60 + "|" + inc;
			}
			String modeStr;
			if (object.getString("variant").equals("standard")) {
				modeStr = object.getString("speed");
				switch (modeStr) {
					case "ultraBullet": case "bullet": modeStr = "bullet"; break;
					case "blitz": modeStr = "blitz"; break;
					case "rapid": case "classical": modeStr = "rapid"; break;
					case "correspondence": modeStr = "untimed"; break;
					default: throw GameProcessor.ParseError.UNKNOWN_MODE;
				}
			} else {
				modeStr = object.getString("variant");
				if (!DataManager.getModeByName(null, modeStr).isPresent()) {
					try {
						Mode newMode = new Mode(null, modeStr, "A Lichess Variant", true);
						DataManager.addMode(newMode);
					} catch (SQLException throwables) {
						throwables.printStackTrace();
					}
				}
			}

			return new LichessGame(white, black, timestamp, modeStr, time, endType, winner, id);
		} catch (JSONException | IOException e) {
			e.printStackTrace();
			throw GameProcessor.ParseError.LICHESS_ERROR;
		}
	}

	/**
	 * FROM https://www.baeldung.com/java-http-request
	 * Method for actually getting data from the API.
	 *
	 * @param urlStr The URL to request from, parameters included.
	 * @return Returns a JSON response from the website.
	 * @throws IOException Occurs when the data is not JSON or the request fails otherwise.
	 */
	private static String readJsonFromUrl(String urlStr) throws IOException {
		URL url = new URL(urlStr);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/json");
		BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuilder content = new StringBuilder();
		while ((inputLine = rd.readLine()) != null) {
			content.append(inputLine);
		}
		rd.close();
		con.disconnect();
		return content.toString();
	}

	@Getter
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class LichessGame {
		private final String white, black;
		/**
		 * Timestamp in epoch millis
		 */
		private final long timestamp;
		private final String mode;
		private final String timeStr;
		private final Game.EndType result;
		private final Game.Winner winner;
		private final String id;
	}
}
